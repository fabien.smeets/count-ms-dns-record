# Script de récupération des informations sur les zones DNS

Ce script PowerShell récupère le nom de chaque zone DNS présente sur le serveur, compte le nombre de records pour chaque zone et affiche la sortie correspondante.

## Instructions d'exécution

1. Assurez-vous que PowerShell est installé sur votre système
2. Ouvrez un terminal PowerShell
3. Copiez et collez le fichier sur le bureau
4. Executer la commande 

`C:\Users\AdministratorDNS\Desktop> .\count_record.ps1`

## Exemple de sortie :
| ZoneName        | RecordCount |
| --------------- | ----------- |
| example.com     | 10          |
| subdomain.example | 5          |
| Total           | 15          |

La colonne "ZoneName" affiche le nom de chaque zone DNS, et la colonne "RecordCount" affiche le nombre de records correspondant à chaque zone. La dernière ligne indique la somme totale de tous les records présents sur le serveur.
