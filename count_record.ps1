$zones = Get-DnsServerZone
$output = @()
$recordCountSum = 0

foreach ($zone in $zones) {
    $zoneName = $zone.ZoneName
    $recordCount = (Get-DnsServerResourceRecord -ZoneName $zoneName).Count
    $zoneInfo = [PSCustomObject]@{
        "ZoneName" = $zoneName
        "RecordCount" = $recordCount
    }
    $output += $zoneInfo
    $recordCountSum += $recordCount
}

$output + [PSCustomObject]@{
    "ZoneName" = "Total"
    "RecordCount" = $recordCountSum
}